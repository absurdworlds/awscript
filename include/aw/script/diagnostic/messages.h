DIAG(error, generic_error, "%0")
DIAG(error, UnexpectedToken, "Unexpected token: expected ‘%0’, got ‘%1’.")
DIAG(error, ExpectedVariableDecl, "Expected variable declaration.")
DIAG(error, ExpectedSemicolonAfterExpression, "Expected ‘;’ after expression.")
